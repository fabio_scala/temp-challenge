const Facade = require('../../lib/facade');
const rateSchema = require('./schema');

class RateFacade extends Facade {
}

module.exports = new RateFacade('Rate', rateSchema);
