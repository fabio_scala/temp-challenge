const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/currencies')
    .get((...args) => controller.findCurrencies(...args));

router.route('/findByCurrency')
    .get((...args) => controller.findByCurrency(...args));

module.exports = router;
