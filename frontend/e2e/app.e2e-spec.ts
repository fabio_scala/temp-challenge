import {AppPageAccessor} from './app.po';

describe('currency-rates App', () => {
  let page: AppPageAccessor;

  beforeEach(() => {
    page = new AppPageAccessor();
  });

  it('should display no rates', () => {
    page.navigateTo();
    page.ratesGrid.containsNoRows();
  });

  it('should display rates after selection', () => {
    page.navigateTo();
    page.rateSelection.selectCurrency('HKD');
    page.ratesGrid.containsRows();
  });
});


