import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Currency} from '../../model/currency';

@Component({
  selector: 'cr-rates-selection',
  templateUrl: './rates-selection.component.html',
  styleUrls: ['./rates-selection.component.scss']
})
export class RatesSelectionComponent {
  @Input()
  currencies: Currency[];

  @Input()
  selectedCurrency: Currency;

  @Output()
  change = new EventEmitter<Currency>();

  onChange() {
    this.change.emit(this.selectedCurrency);
  }
}
