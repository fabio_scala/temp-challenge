const Controller = require('../../lib/controller');
const rateFacade = require('./facade');

class RateController extends Controller {
    findCurrencies(req, res, next) {
        return this.facade.model.aggregate([{$sort: {currency: -1}}, {
            $group: {
                _id: "$key",
                currencies: {$addToSet: "$currency"}
            }
        }])
            .then(collection => res.status(200).json(collection[0].currencies))
            .catch(err => next(err));
    }

    findByCurrency(req, res, next) {
        return this.facade.find({currency: req.query.currency})
            .then(collection => res.status(200).json(collection))
            .catch(err => next(err));
    }
}

module.exports = new RateController(rateFacade);
