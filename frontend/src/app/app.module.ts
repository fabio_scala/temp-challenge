import {BrowserModule} from '@angular/platform-browser';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';


import {AppComponent} from './app.component';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {DataTableModule, DropdownModule} from 'primeng/primeng';

import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './core/service/api.service';
import {RatesGridComponent} from './component/rates-grid/rates-grid.component';
import {RatesSelectionComponent} from './component/rates-selection/rates-selection.component';


@NgModule({
  declarations: [
    AppComponent,
    RatesGridComponent,
    RatesSelectionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SlimLoadingBarModule.forRoot(),
    DropdownModule,
    DataTableModule

  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
