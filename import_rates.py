#!/usr/bin/env python3

import datetime
import time
from pymongo import MongoClient
from forex_python.converter import CurrencyRates, RatesNotAvailableError


FROM = datetime.datetime(2017, 1, 1)
TO = datetime.datetime.today()
RATE_LIMIT_TIMEOUT_S = 10


client = MongoClient('localhost', 27017)
db = client['rate']
rates_client = CurrencyRates()

date = FROM
while date <= TO:
    print('Importing {}...'.format(date.strftime('%Y-%m-%d')))
    try:
        rates = [{'date': date, 'currency': currency, 'price': price} for currency, price in rates_client.get_rates('USD', date).items()]
        db.rates.delete_many({'date': date})
        db.rates.insert_many(rates)
        date = date + datetime.timedelta(days=1)
    except RatesNotAvailableError:
        print('Rate limit reached. Sleeping for {} seconds...'.format(RATE_LIMIT_TIMEOUT_S))
        time.sleep(RATE_LIMIT_TIMEOUT_S)
