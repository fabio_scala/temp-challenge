import {browser, by, element} from 'protractor';

export class AppPageAccessor {

  ratesGrid = new RatesGridAccessor();
  rateSelection = new RateSelectionAccessor();

  navigateTo() {
    return browser.get('/');
  }
}


export class RateSelectionAccessor {
  selectCurrency(currency: string) {
    const currencyInput = element(by.id('currency-selection'));
    currencyInput.click();
    element(by.cssContainingText('.ui-dropdown-item span', currency)).click();
  }
}


export class RatesGridAccessor {
  get noRowsIndicator() {
    return element(by.cssContainingText('span', 'No records found'));
  }

  containsNoRows() {
    expect(this.noRowsIndicator.isDisplayed()).toBeTruthy();
  }

  containsRows() {
    expect(this.noRowsIndicator.isPresent()).toBeFalsy();
  }
}
