import {Injectable} from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

  private currentRequests = 0;

  constructor(private http: HttpClient, private loadingBar: SlimLoadingBarService) {
  }

  getUrl(path: string) {
    return `${environment.apiUrl}/${path}`;
  }

  get(path: string) {
    const url = this.getUrl(path);
    this.showLoadingBar();
    return this.wrapResponse(this.http.get(url));
  }

  post(path: string, body: any) {
    this.showLoadingBar();
    const url = this.getUrl(path);
    return this.wrapResponse(this.http.post(url, body));
  }

  private wrapResponse(response: Observable<Object>) {
    response.subscribe(() => {
    }, () => {
    }, () => {
      this.hideLoadingBar();
    });
    return response;
  }

  private showLoadingBar() {
    this.currentRequests++;
    this.loadingBar.start();
  }

  private hideLoadingBar() {
    this.currentRequests--;
    if (this.currentRequests <= 0) {
      this.loadingBar.complete();
      this.currentRequests = 0;
    }
  }
}
