import {Component, Input, OnInit} from '@angular/core';
import {Rate} from '../../model/rate';

@Component({
  selector: 'cr-rates-grid',
  templateUrl: './rates-grid.component.html',
  styleUrls: ['./rates-grid.component.scss']
})
export class RatesGridComponent implements OnInit {

  @Input()
  rates = [] as Rate[];

  constructor() {
  }

  ngOnInit() {
  }

}
