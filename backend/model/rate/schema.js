const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const rateSchema = new Schema({
    currency: {type: String, required: true},
    date: {type: Date, required: true},
    price: {type: Number, required: true}
});


module.exports = rateSchema;
