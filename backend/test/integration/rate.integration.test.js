const should = require('should'),
    request = require('supertest'),
    app = require('../../index.js'),
    mongoose = require('mongoose'),
    Rate = mongoose.model('Rate'),
    agent = request.agent(app);

describe('Rate integration testing', () => {
    it('Should get all currencies', (done) => {
        agent
            .get('/rate/currencies')
            .expect(200)
            .end((err, result) => {
                result.body.should.be.eql(['BGN', 'BRL', 'CHF', 'CNY', 'CZK', 'GBP', 'HKD', 'HUF', 'IDR', 'ILS', 'INR',
                                           'CAD', 'MXN', 'MYR', 'AUD', 'SGD', 'NZD', 'NOK', 'PLN', 'RON', 'DKK', 'RUB',
                                           'EUR', 'KRW', 'PHP', 'SEK', 'HRK', 'THB', 'TRY', 'JPY', 'ZAR']);
                done();
            });
    });

    it('Should get all rates by currency', (done) => {
        agent
            .get('/rate/findByCurrency?currency=HKD')
            .expect(200)
            .end((err, result) => {
                result.body.length.should.be.greaterThan(0);
                let rate = result.body[0];
                rate.currency.should.eql('HKD');
                rate.should.have.property('price').which.is.a.Number();
                rate.should.have.property('date').which.is.a.String();
                done();
            });
    });
});
