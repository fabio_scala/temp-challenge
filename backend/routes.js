const Router = require('express').Router;
const router = new Router();

const rate = require('./model/rate/router');

router.route('/').get((req, res) => {
    res.json({message: 'Currency Exchange API'});
});

router.use('/rate', rate);

module.exports = router;
