import {Component} from '@angular/core';
import {ApiService} from './core/service/api.service';
import {Currency} from './model/currency';
import {Rate} from './model/rate';

@Component({
  selector: 'cr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  currencies = [];
  rates = [];


  constructor(private api: ApiService) {
    this.api.get('rate/currencies').subscribe((data: string[]) => {
      this.currencies = data.map(currencyCode => Object.assign(new Currency(), {currencyCode}));
    });
  }

  onCurrencyChange(currency: Currency) {
    this.api.get(`rate/findByCurrency?currency=${currency.currencyCode}`).subscribe((data: object[]) => {
      this.rates = data.map(rate => Object.assign(new Rate(), rate));
    });
  }
}
