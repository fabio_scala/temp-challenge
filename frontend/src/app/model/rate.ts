export class Rate {
  date: Date;
  currency: string;
  price: number;
}
