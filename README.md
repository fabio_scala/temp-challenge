# Currency Exchange Rates

![alt text](screenshot.png)

This is a minimal front- & backend project which display the historical exchange rates of any selected currency vs. USD.

* Invested time: ~6h
* No prior Node.js knowledge
* No prior MongoDB knowledge

## Backend
* Node.js as required by the challenge
* Seed project generated with Yeoman "generator-api"
  * ./lib/* is came with the seed project (generic controller & facade for data access)
* Security
  * Removed direct access to facade, only expose route to required methods
  * TODO: Configure CORS only for the official client in production
* Testing
  * No unit test, as there is no business logic in JS
  * Few simple integration tests which require a database
* Other TODO
  * Server-side pagination
  * Fetch live/real-time rates?
  
For development (localhost:8080): 
```bash
yarn install
npm run
```

Run using docker-compose:
```bash
sudo docker-compose up
```

Run tests:
```bash
sudo npm install -g istanbul
yarn install
npm test
```

## Frontend
* Angular 5, initialized using Angular CLI
* Prior Angular knowledge, which is why I did not go for React
* React is quite low-level, only UI/templating, while Angular is all-in-batteries-included (components, models, testing, etc.)
* Testing
  * Few simple integration tests using PageObject/Accessor pattern
  * TODO: No unit tests of the components 
* Other TODO
  * Did not use ngrx (redux) for this very simple one-page app
  * Error handling (in this case: response from server)
  
For development (localhost:4200): 
```bash
sudo npm install -g @angular/cli
yarn install
ng serve
```

Run integration tests:
```bash
yarn install
npm run e2e
```

## Database
* MongoDB perfect fit for Node.js
  * No strong consistency (and ACID properties) required, BASE is enough
* Rate are imported by a simple Python script (see import_rates.py`)`
* Alternative: instead of pre-populating data, fetch and cache as it is requested
  * Pros: No external scheduled scripts, always up-to-date date, realtime rate
  * Cons: APIs have rate limits, user/call needs to wait long time
  * Better: Both, have a scheduled import for daily rates and fetch the latest/realtime rate directly.
